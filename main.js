import './style.css'

let hide = true;
const more = document.querySelector('#more');

more.addEventListener("click", () => {
	if (hide === true) {
		hide = false;
		document.querySelector('.more').style.display = "block";
		more.innerText = "أقل";
	} else {
		hide = true;
		document.querySelector('.more').style.display = "none";
		more.innerText = "المزيد ...";
	}
});
